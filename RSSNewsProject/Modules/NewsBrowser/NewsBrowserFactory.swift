//
//  NewsBrowserFactory.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

struct NewsBrowserFactory {

    private static let _shared = NewsBrowserFactory()

    static var shared: NewsBrowserFactory {
        return _shared
    }


    func createModule(with path: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardId, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: initialControllerID) as! NewsBrowserViewController
        let presenter = NewsBrowserPresenter()
        presenter.view = controller
        presenter.path = path
        let interactor = NewsBrowserInteractor()
        let router         = NewsBrowserRouter()
        presenter.router = router
        router.viewController = controller
        presenter.interactor = interactor
        interactor.output = presenter
        controller.output = presenter
        return controller
    }
    
    // MARK:
    let storyboardId = "NewsBrowser"
    let initialControllerID = "NewsBrowserViewController"
}
