//
//  NewsBrowserInteractor.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class NewsBrowserInteractor {
	weak var output: NewsBrowserInteractorOutput?
}

/**
 * Presenter -> Interactor
 */
extension NewsBrowserInteractor: NewsBrowserInteractorInput {
	
}
