//
//  NewsBrowserInteractorInput.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

/**
 * Interactor -> Presenter
 */
protocol NewsBrowserInteractorOutput: class {

}

/**
 * Presenter -> Interactor
 */
protocol NewsBrowserInteractorInput {

}
