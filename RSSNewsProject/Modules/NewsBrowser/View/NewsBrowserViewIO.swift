//
//  NewsBrowserViewIO.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

/**
 *  Presenter -> View
 */
protocol NewsBrowserViewInput: class {
    func openBrowserBy(_ path: String)
}

/**
 * View -> Presenter
 */
protocol NewsBrowserViewOutput {

  	func viewIsReady()

}
