//
//  NewsBrowserViewController.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit
import WebKit

class NewsBrowserViewController: UIViewController, WKNavigationDelegate {

    // MARK: Outlets
    @IBOutlet var webView: WKWebView!
    
    // MARK: Dependencies
    var output: NewsBrowserViewOutput!
    
   	// MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    // MARK: Actions
    
    //MARK:- WKNavigationDelegate
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start to load")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finish to load")
    }
    
}

// MARK:
extension NewsBrowserViewController: NewsBrowserViewInput {
    func openBrowserBy(_ path: String) {
        if let url = URL(string: path) {
            let request = URLRequest(url: url)
            webView = WKWebView(frame: self.view.frame)
            webView.navigationDelegate = self
            webView.load(request)
            self.view = webView
        }
    }
}
