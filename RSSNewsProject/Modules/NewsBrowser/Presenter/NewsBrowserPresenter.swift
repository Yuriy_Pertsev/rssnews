//
//  NewsBrowserPresenter.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class NewsBrowserPresenter {

    weak var view: NewsBrowserViewInput!
    var interactor: NewsBrowserInteractorInput!
    var router: NewsBrowserRouterInput!
    
    var path: String!
}

// MARK: ViewOutput
extension NewsBrowserPresenter: NewsBrowserViewOutput {

    func viewIsReady() {
            view.openBrowserBy(path)
    }

}

// MARK: InteractorOutput
extension NewsBrowserPresenter: NewsBrowserInteractorOutput {


}


