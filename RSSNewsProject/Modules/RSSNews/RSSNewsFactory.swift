//
//  RSSNewsFactory.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

struct RSSNewsFactory {

    private static let _shared = RSSNewsFactory()

    static var shared: RSSNewsFactory {
        return _shared
    }


    func createModule() -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardId, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: initialControllerID) as! RSSNewsViewController
        let presenter = RSSNewsPresenter()
        presenter.view = controller
        
        let interactor = RSSNewsInteractor()
        let router         = RSSNewsRouter()
        presenter.router = router
        router.viewController = controller
        presenter.interactor = interactor
        interactor.output = presenter
        controller.output = presenter
        return controller
    }
    
    // MARK:
    let storyboardId = "RSSNews"
    let initialControllerID = "RSSNewsViewController"
}
