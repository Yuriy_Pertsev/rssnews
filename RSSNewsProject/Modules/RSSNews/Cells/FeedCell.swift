//
//  NewsCell.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/22/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {

    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var feedTitleLabel: UILabel!
    @IBOutlet weak var feedDescriptionLabel: UILabel!
    
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    
    var feedController: RSSNewsViewController?
    var isZooming = true
    var originalImageCenter:CGPoint?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        feedImage.isUserInteractionEnabled = true
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(sender:)))
        pan.delegate = self
        self.feedImage.addGestureRecognizer(pan)
        
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillCell(_ feed: Feed) {
        feedTitleLabel.text = feed.title
        feedDescriptionLabel.text = feed.body
        
        if feed.imagePath == nil {
          feedImage.image = nil
          imageWidthConstraint.constant = 0
        } else {
            
            DispatchQueue.global(qos: .background).async { [weak feed] in
                var imageData: Data? = nil
                if let imagePath = feed?.imagePath {
                    imageData = ImageLoader.shared.getImage(by: imagePath)
                }
                DispatchQueue.main.async {
                    if let imageData = imageData {
                        self.feedImage.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }
    
    
    @objc func pan(sender: UIPanGestureRecognizer) {
        feedController?.animateImageView(imageView: feedImage)
    }
    
}
