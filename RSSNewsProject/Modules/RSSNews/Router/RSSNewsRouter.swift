//
//  RSSNewsRouter.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit
import Foundation

class RSSNewsRouter {

	weak var viewController : UIViewController?

}

extension RSSNewsRouter: RSSNewsRouterInput {

    func openFeedDetails(feed: Feed) {
        let factory = NewsDetailsFactory.shared
        let vc = factory.createModule(with: feed)
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
