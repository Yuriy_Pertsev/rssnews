//
//  RSSNewsViewController.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

let feedCellIdentifier = "FeedCell"

class RSSNewsViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var feedsTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Dependencies
    var output: RSSNewsViewOutput!
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredFeeds = [Feed]()
    
    
    var feeds: [Feed]! {
        didSet {
            stopActivityIndicator()
            feedsTableView.reloadData()
        }
    }
    
   	// MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        feedsTableView.register(UINib(nibName: feedCellIdentifier, bundle: nil),
                                     forCellReuseIdentifier: feedCellIdentifier)
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("FIND_FEED", comment: "")
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        output.viewIsReady()
        startActivityIndicator()
        output.getFeeds()
        
        let rightBarButton = UIBarButtonItem(title: "RSS", style: .plain, target: self, action: #selector(setRssUrl(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    @objc func setRssUrl(_ sender:UIBarButtonItem!)
    {
        let ac = UIAlertController(title: NSLocalizedString("ENTER_URL", comment: ""), message: nil, preferredStyle: .alert)
        ac.addTextField()
        
        let submitAction = UIAlertAction(title: NSLocalizedString("SUBMIT", comment: ""), style: .default) { [unowned ac, weak self] _ in
            let answer = ac.textFields![0]
            PreferencesSerialization.writeServiceURL(answer.text ?? "")
            self?.output.getFeeds()
        }
        
        ac.addAction(submitAction)
        
        present(ac, animated: true)
        
    }
    
    // MARK: Actions
    func startActivityIndicator() {
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func updateUserInterface() {
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
        
        if !Network.reachability.isReachable {
            showError(NSLocalizedString("NETWORK_ISSUE", comment: ""))
        }
    }

    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    lazy var zoomImageView = UIImageView()
    var feedImageView: UIImageView!
    var isZooming = false
    
    func animateImageView(imageView: UIImageView) {
        feedImageView = imageView
        if let startingFrame = imageView.superview?.convert(imageView.frame, to: nil) {
            
            imageView.alpha = 0
            
            zoomImageView.frame = startingFrame
            zoomImageView.isUserInteractionEnabled = true
            zoomImageView.image = imageView.image
            zoomImageView.contentMode = .scaleAspectFill
            zoomImageView.clipsToBounds = true
            self.view.addSubview(zoomImageView)
            
            zoomImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.zoomOut(sender:))))
            
            zoomImageView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(self.pinch(sender:))))
            
            UIView.animate(withDuration: 0.75) { [weak self] () -> Void in
                let height = self!.view.frame.height
                self?.zoomImageView.frame = CGRect(x: 0, y: 0, width: self!.view.frame.width, height: height)
                
                if let keyWindow = UIApplication.shared.keyWindow {
                    keyWindow.addSubview(self!.zoomImageView)
                }
            }
        }
    }
    
    @objc func zoomOut(sender: UIPanGestureRecognizer) {
        if let startingFrame = feedImageView.superview?.convert(feedImageView.frame, to: nil) {
            
            UIView.animate(withDuration: 0.75, animations: {
                self.zoomImageView.frame = startingFrame
            }) { (didComplete) in
                self.zoomImageView.removeFromSuperview()
                self.feedImageView.alpha = 1
            }
        }
    }
    
    @objc func pinch(sender:UIPinchGestureRecognizer) {
        
        if sender.state == .began {
            let currentScale = self.zoomImageView.frame.size.width / self.zoomImageView.bounds.size.width
            let newScale = currentScale*sender.scale
            
            if newScale > 1 {
                self.isZooming = true
            }
        } else if sender.state == .changed {
            
            guard let view = sender.view else {return}
            
            let pinchCenter = CGPoint(x: sender.location(in: view).x - view.bounds.midX,
                                      y: sender.location(in: view).y - view.bounds.midY)
            let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                .scaledBy(x: sender.scale, y: sender.scale)
                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
            
            let currentScale = self.zoomImageView.frame.size.width / self.zoomImageView.bounds.size.width
            var newScale = currentScale*sender.scale
            
            if newScale < 1 {
                newScale = 1
                let transform = CGAffineTransform(scaleX: newScale, y: newScale)
                self.zoomImageView.transform = transform
                sender.scale = 1
            }else {
                view.transform = transform
                sender.scale = 1
            }
            
        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            
            
            if let startingFrame = feedImageView.superview?.convert(feedImageView.frame, to: nil) {
            
                UIView.animate(withDuration: 0.3, animations: {
                    self.zoomImageView.transform = CGAffineTransform.identity
                    self.zoomImageView.center = startingFrame.origin
                }, completion: { _ in
                    self.isZooming = false
                })
            }
        }
        
    }
}

// MARK:
extension RSSNewsViewController: RSSNewsViewInput {
    func showError(_ message: String) {
        let alertVC = UIAlertController(title: NSLocalizedString("WARNING", comment: ""), message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil)
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showReceivedFeeds(_ feeds: [Feed]) {
        self.feeds = feeds
    }
}

extension RSSNewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering() {
            return filteredFeeds.count
        } else {
            return self.feeds != nil ? self.feeds.count : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        guard let cell = tableView.dequeueReusableCell(withIdentifier: feedCellIdentifier,
                                                       for: indexPath) as? FeedCell else { return UITableViewCell() }
        
        if isFiltering() {
            cell.fillCell(self.filteredFeeds[indexPath.row])
        } else {
            cell.fillCell(self.feeds[indexPath.row])
        }
        cell.feedController = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let feed = self.feeds[indexPath.row]
        output.feedSelected(feed)
    }
    
}

extension RSSNewsViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredFeeds = feeds.filter({( feed : Feed) -> Bool in
            return feed.title!.lowercased().contains(searchText.lowercased())
        })
        feedsTableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
}
