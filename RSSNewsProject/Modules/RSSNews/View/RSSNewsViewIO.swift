//
//  RSSNewsViewIO.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

/**
 *  Presenter -> View
 */
protocol RSSNewsViewInput: class {
    func showError(_ message: String)
    func showReceivedFeeds(_ feeds: [Feed])
}

/**
 * View -> Presenter
 */
protocol RSSNewsViewOutput {

  	func viewIsReady()
    func getFeeds()
    func feedSelected(_ feed: Feed)
    

}
