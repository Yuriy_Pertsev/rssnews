//
//  RSSNewsInteractor.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class RSSNewsInteractor {
	weak var output: RSSNewsInteractorOutput?
}

/**
 * Presenter -> Interactor
 */
extension RSSNewsInteractor: RSSNewsInteractorInput {
    
    func getNewFeeds() {
        let rssPath = PreferencesSerialization.getRSSServiceURL(withName: "Preferences") 
        let parser = RSSParser()
        if let rssPath = rssPath, let rssURL = URL(string: rssPath) {
            print("get feed by: \(rssPath)")
            parser.startParsingWithContentsOfURL(rssURL: rssURL) { [weak self] result in
                if result == true {
                    parser.parsedData.remove(at: 0)
                    parser.parsedData.remove(at: 0)
                    self?.saveInCoreDataWith(array: parser.parsedData)
                } else {
                    output?.showError(NSLocalizedString("GETTING_FEED_FAILED", comment: ""))
                }
            }
        }
    }
    
    private func saveInCoreDataWith(array: [[String: String]]) {
        
        if Network.reachability.isReachable {
            CoreDataStack.sharedInstance.deleteAllFeedsInContext()
        }
        _ = array.map{CoreDataStack.sharedInstance.createFeedEntityFrom(dictionary: $0)}
        do {
            try CoreDataStack.sharedInstance.persistentContainer.viewContext.save()
            output?.feedsSavedToCoreData()
        } catch let error {
            print(error)
        }
    }
    
    func getSavedFeeds() {
        if let feeds = CoreDataStack.sharedInstance.getFeeds() {
            output?.receivedFeeds(feeds)
        } else {
            output?.showError(NSLocalizedString("GETTING_LOCAL_FEED_FAILED", comment: ""))
        }
        
    }
	
}
