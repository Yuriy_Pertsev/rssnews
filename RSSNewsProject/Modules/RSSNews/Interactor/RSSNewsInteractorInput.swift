//
//  RSSNewsInteractorInput.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

/**
 * Interactor -> Presenter
 */
protocol RSSNewsInteractorOutput: class {

    func showError(_ message: String)
    func feedsSavedToCoreData()
    func receivedFeeds(_ feeds: [Feed])
}

/**
 * Presenter -> Interactor
 */
protocol RSSNewsInteractorInput {
    
    func getNewFeeds()
    func getSavedFeeds()

}
