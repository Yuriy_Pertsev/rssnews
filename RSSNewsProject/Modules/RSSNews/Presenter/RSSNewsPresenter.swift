//
//  RSSNewsPresenter.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class RSSNewsPresenter {

    weak var view: RSSNewsViewInput!
    var interactor: RSSNewsInteractorInput!
    var router: RSSNewsRouterInput!
}

// MARK: ViewOutput
extension RSSNewsPresenter: RSSNewsViewOutput {
    
    func feedSelected(_ feed: Feed) {
        router.openFeedDetails(feed: feed)
    }
    
    func getFeeds() {
        if  Network.reachability.isReachable {
            interactor.getNewFeeds()
        } else {
            interactor.getSavedFeeds()
        }
    }
    
    func viewIsReady() {
            
    }
}

// MARK: InteractorOutput
extension RSSNewsPresenter: RSSNewsInteractorOutput {
    func feedsSavedToCoreData() {
        interactor.getSavedFeeds()
    }
    
    func receivedFeeds(_ feeds: [Feed]) {
        view.showReceivedFeeds(feeds)
    }
    
    func showError(_ message: String) {
        view.showError(message)
    }
    
}


