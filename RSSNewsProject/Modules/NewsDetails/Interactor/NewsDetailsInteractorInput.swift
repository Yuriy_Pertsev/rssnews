//
//  NewsDetailsInteractorInput.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

/**
 * Interactor -> Presenter
 */
protocol NewsDetailsInteractorOutput: class {

}

/**
 * Presenter -> Interactor
 */
protocol NewsDetailsInteractorInput {

}
