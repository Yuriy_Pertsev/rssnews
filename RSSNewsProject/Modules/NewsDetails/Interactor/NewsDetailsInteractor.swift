//
//  NewsDetailsInteractor.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class NewsDetailsInteractor {
	weak var output: NewsDetailsInteractorOutput?
}

/**
 * Presenter -> Interactor
 */
extension NewsDetailsInteractor: NewsDetailsInteractorInput {
	
}
