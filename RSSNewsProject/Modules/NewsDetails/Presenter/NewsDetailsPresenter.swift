//
//  NewsDetailsPresenter.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class NewsDetailsPresenter {

    weak var view: NewsDetailsViewInput!
    var interactor: NewsDetailsInteractorInput!
    var router: NewsDetailsRouterInput!
    
    var feed: Feed!
    
}

// MARK: ViewOutput
extension NewsDetailsPresenter: NewsDetailsViewOutput {
    
    func openBrowserPressed() {
        router.openBrowserWith(feed.link ?? "")
    }
    

    func viewIsReady() {
            view.showFeedInformation(feed)
    }

}

// MARK: InteractorOutput
extension NewsDetailsPresenter: NewsDetailsInteractorOutput {


}


