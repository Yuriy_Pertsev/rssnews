//
//  NewsDetailsViewIO.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

/**
 *  Presenter -> View
 */
protocol NewsDetailsViewInput: class {

    func showFeedInformation(_ feed: Feed)
}

/**
 * View -> Presenter
 */
protocol NewsDetailsViewOutput {

  	func viewIsReady()
    func openBrowserPressed()

}
