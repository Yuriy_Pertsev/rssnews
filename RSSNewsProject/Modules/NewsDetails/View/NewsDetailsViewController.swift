//
//  NewsDetailsViewController.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

class NewsDetailsViewController: UIViewController {

    // MARK: Outlets
    
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var navigationItemTitle: UINavigationItem!
    // MARK: Dependencies
    var output: NewsDetailsViewOutput!
    
   	// MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        output.viewIsReady()
    }
    
    func setLongerTitle(_ text: String) {
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = text
        self.navigationItemTitle.titleView = label
        
    }

    // MARK: Actions
    @IBAction func openBrowserPressed(_ sender: Any) {
        output.openBrowserPressed()
    }
    
}

// MARK:
extension NewsDetailsViewController: NewsDetailsViewInput {
    
    func showFeedInformation(_ feed: Feed) {
        
        titleLabel.text = feed.title
        descriptionLabel.text = feed.body
        setLongerTitle(feed.title ?? "Details")
        
        if feed.imagePath == nil {
            feedImage.image = nil
        } else {
            
            DispatchQueue.global(qos: .background).async { [weak feed] in
                var imageData: Data? = nil
                if let imagePath = feed?.imagePath {
                    imageData = ImageLoader.shared.getImage(by: imagePath)
                }
                DispatchQueue.main.async {
                    if let imageData = imageData {
                        self.feedImage.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }

}
