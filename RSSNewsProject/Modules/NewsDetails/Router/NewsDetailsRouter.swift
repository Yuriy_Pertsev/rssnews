//
//  NewsDetailsRouter.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit
import Foundation

class NewsDetailsRouter {

	weak var viewController : UIViewController?

}

extension NewsDetailsRouter: NewsDetailsRouterInput {

    func openBrowserWith(_ path: String) {
        let factory = NewsBrowserFactory.shared
        let vc = factory.createModule(with: path)
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
