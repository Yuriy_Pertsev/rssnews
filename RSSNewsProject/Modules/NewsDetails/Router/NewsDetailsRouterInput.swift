//
//  NewsDetailsRouterInput.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 22/05/2019.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

protocol NewsDetailsRouterInput {

    func openBrowserWith(_ path: String)
    
}
