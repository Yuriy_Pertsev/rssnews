//
//  NewsDetailsFactory.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/21/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

struct NewsDetailsFactory {

    private static let _shared = NewsDetailsFactory()

    static var shared: NewsDetailsFactory {
        return _shared
    }


    func createModule(with feed: Feed) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardId, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: initialControllerID) as! NewsDetailsViewController
        let presenter = NewsDetailsPresenter()
        presenter.view = controller
        presenter.feed = feed
        let interactor = NewsDetailsInteractor()
        let router         = NewsDetailsRouter()
        presenter.router = router
        router.viewController = controller
        presenter.interactor = interactor
        interactor.output = presenter
        controller.output = presenter
        return controller
    }
    
    // MARK:
    let storyboardId = "NewsDetails"
    let initialControllerID = "NewsDetailsViewController"
}
