// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Feed.swift instead.

import Foundation
import CoreData

public enum FeedAttributes: String {
    case body = "body"
    case imageData = "imageData"
    case imagePath = "imagePath"
    case link = "link"
    case pubDate = "pubDate"
    case title = "title"
}

open class _Feed: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Feed"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Feed.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var body: String?

    @NSManaged open
    var imageData: NSData?

    @NSManaged open
    var imagePath: String?

    @NSManaged open
    var link: String?

    @NSManaged open
    var pubDate: Date?

    @NSManaged open
    var title: String?

    // MARK: - Relationships

}

