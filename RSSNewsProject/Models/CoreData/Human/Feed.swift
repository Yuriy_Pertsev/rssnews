import Foundation

enum FeedFields: String {
    case title = "title"
    case description = "description"
    case imagePath = "media:content"
    case link = "link"
    case pubDate = "pubDate"
}

@objc(Feed)
open class Feed: _Feed {
	// Custom logic goes here.
}

