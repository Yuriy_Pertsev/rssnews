//
//  ValuesConverter.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/22/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation


class ValuesConverter {

    static func getFullDateFromString(_ dateString: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "GMT")
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        
        return dateFormatter.date(from: dateString)
    }

}
