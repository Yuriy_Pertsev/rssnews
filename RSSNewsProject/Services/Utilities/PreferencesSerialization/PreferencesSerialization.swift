//
//  FileSerialization.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/23/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

let preferencesFileName = "Preferences"

struct Preferences: Codable {
    var rssServiceURL:String
}

class PreferencesSerialization {
    
   static func getRSSServiceURL(withName name: String) -> String?
    {
        if  let path = Bundle.main.path(forResource: name, ofType: "plist"),
            let xml = FileManager.default.contents(atPath: path),
            let preferences = try? PropertyListDecoder().decode(Preferences.self, from: xml)
        {
            return preferences.rssServiceURL
        }
        return nil
    }
    
    static func writeServiceURL(_ rssPath: String) {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        
        let url = Bundle.main.url(forResource: preferencesFileName, withExtension: "plist")
        
            let preferences = Preferences(rssServiceURL: rssPath)

            do {
                let data = try encoder.encode(preferences)
                try data.write(to: url!)
            } catch {
                print(error)
            }
        }
}
