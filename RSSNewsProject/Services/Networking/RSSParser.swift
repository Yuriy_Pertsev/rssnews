//
//  RSSParser.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/22/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

class RSSParser: NSObject, XMLParserDelegate {
    
    var xmlParser: XMLParser!
    var currentElement =  ""
    var foundCharacters = ""
    var currentData = [String : String]()
    var parsedData = [[String: String]]()
    var isHeader = true
    
    func startParsingWithContentsOfURL(rssURL: URL, with completion: (Bool)->()) {
        
        let parser = XMLParser(contentsOf: rssURL)
        parser?.delegate = self
        if let flag = parser?.parse() {
            parsedData.append(currentData)
            completion(flag)
        }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        if currentElement == "title" {
            if isHeader == false {
                parsedData.append(currentData)
            }
            isHeader = false
        }
        
        if isHeader == false {
            if currentElement == "media:thumbnail" || currentElement == "media:content" {
                foundCharacters += attributeDict["url"]!
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        if isHeader == false {
            if currentElement == "title" || currentElement == "link" || currentElement == "description" || currentElement == "content" || currentElement == "image" || currentElement == "url" || currentElement == "pubDate" || currentElement == "author" {
                foundCharacters += string
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if !foundCharacters.isEmpty {
            foundCharacters = foundCharacters.trimmingCharacters(in: .whitespacesAndNewlines)
            currentData[currentElement] = foundCharacters
            foundCharacters = ""
        }
    }
    
}
