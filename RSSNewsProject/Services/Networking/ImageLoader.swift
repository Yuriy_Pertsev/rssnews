//
//  ImageLoader.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/22/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import UIKit

class ImageLoader: NSObject {
    
    static let shared = ImageLoader()
    
    func getImage(by path: String) -> Data? {
        if let imageUrl = URL(string: path) {
            let imageData = try? Data(contentsOf: imageUrl)
            return imageData
        }
        return nil
    }
}
