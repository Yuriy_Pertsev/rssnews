//
//  CoreDataStack.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/22/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack: NSObject {
    
    static let sharedInstance = CoreDataStack()
    private override init() {}

    func createFeedEntityFrom(dictionary: [String: String]) -> Feed? {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        if let feedEntity = NSEntityDescription.insertNewObject(forEntityName: "Feed", into: context) as? Feed {
            feedEntity.body = dictionary[FeedFields.description.rawValue]
            feedEntity.title = dictionary[FeedFields.title.rawValue]
            feedEntity.link = dictionary[FeedFields.link.rawValue]
            feedEntity.imagePath = dictionary[FeedFields.imagePath.rawValue]
            if let dateString = dictionary[FeedFields.pubDate.rawValue] {
                let date = ValuesConverter.getFullDateFromString(dateString)
                feedEntity.pubDate = date
            } else {
                feedEntity.pubDate = Date()
            }
            
            return feedEntity
        }
        return nil
    }
    
    func deleteAllFeedsInContext() {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Feed.entityName())
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.persistentStoreCoordinator?.execute(deleteRequest, with: context)
        } catch let error as NSError {
            print(NSLocalizedString("NOT_DELETED", comment: "") + "\(error)")
        }
    }
    
    func getFeeds() -> [Feed]? {

        var feeds: [Feed]?
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Feed.entityName())
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: FeedFields.pubDate.rawValue, ascending: false)]

        context.performAndWait {
            feeds = (try? context.fetch(fetchRequest)) as? [Feed]
        }

        return feeds
    }
    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "RSSNewsProject")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
               
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
}
