//
//  Network.swift
//  RSSNewsProject
//
//  Created by Yuriy Pertsev on 5/23/19.
//  Copyright © 2019 Yuriy.Pertsev. All rights reserved.
//

import Foundation

struct Network {
    static var reachability: Reachability!
    enum Status: String {
        case unreachable, wifi, wwan
    }
    enum Error: Swift.Error {
        case failedToSetCallout
        case failedToSetDispatchQueue
        case failedToCreateWith(String)
        case failedToInitializeWith(sockaddr_in)
    }
}
